

(function() {

  // animate logo in from the top
  $('.logo').animate({
    'margin-top': '292px'
  }, 700, function() {

    // then animate ribbon-01 in from the top
    $('.ribbon-01').animate({
      'margin-top': '0'
    }, 700, function() {

      // then animate ribbon-02 in from the top
      $('.ribbon-02').animate({
        'margin-top': '-3px'
      }, 700, function() {

        // then animate ribbon-03 in from the top
        $('.ribbon-03').animate({
          'margin-top': '-3px'
        }, 700);

      });

    });

  });

})();

